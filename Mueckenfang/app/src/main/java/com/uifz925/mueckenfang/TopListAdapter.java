package com.uifz925.mueckenfang;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class TopListAdapter extends ArrayAdapter<String> {

    private List<String> highscoreList = new ArrayList<>();
    private Context context;
    private MueckenfangActivity mActivity;


    public TopListAdapter(Context context, int resource, List<String> highscoreList) {
        super(context, resource);
        this.highscoreList = highscoreList;
    }

    @Override
    public int getCount() {
        int size = highscoreList.size();
        return size;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        if (convertView == null) {
            convertView =  LayoutInflater.from(container.getContext()).inflate(R.layout.toplist_element, null);
        }

        TextView tvPlatz = (TextView) convertView.findViewById(R.id.platz);
        tvPlatz.setText(Integer.toString(position + 1) + ".");
        TextUtils.SimpleStringSplitter sss = new TextUtils.SimpleStringSplitter(',');
        sss.setString(highscoreList.get(position));

        TextView tvName = (TextView) convertView.findViewById(R.id.name);
        tvName.setText(sss.next());

        TextView tvPunkte = (TextView) convertView.findViewById(R.id.punkte);
        if (sss.hasNext()) {
            tvPunkte.setText(sss.next());
        } else {
            tvPunkte.setText("000000");
        }
        return convertView;
    }
}
