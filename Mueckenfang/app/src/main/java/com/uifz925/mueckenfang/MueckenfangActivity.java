package com.uifz925.mueckenfang;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class MueckenfangActivity extends AppCompatActivity implements View.OnClickListener, Html.ImageGetter {

    private static final int REQUESTCODE_PERMISSIONS = 665;
    private TopListAdapter adapter;

    private LinearLayout namenseingabe;

    private Button btnStart;
    private Button btnSpeichern;

    private TextView tv;
    private ListView listView;

    private static final String HIGHSCORE_SERVER_BASE_URL = "https://myhighscoreserver.appspot.com/highscoreserver";
    private static final String HIGHSCORESERVER_GAME_ID = "mueckenfang";

    private URL url;
    private String highscoresHtml;

    private Spinner schwierigkeitsgrad;
    private ArrayAdapter<String> schwierigkeitsgradAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart = (Button) findViewById(R.id.btnStart);

        namenseingabe = (LinearLayout) findViewById(R.id.namenseingabe);
        namenseingabe.setVisibility(View.INVISIBLE);

        btnStart.setOnClickListener(this);
        btnSpeichern = (Button) findViewById(R.id.btnSpeichern);
        btnSpeichern.setOnClickListener(this);

        listView = (ListView) findViewById(R.id.listScores);

        schwierigkeitsgrad = (Spinner) findViewById(R.id.schwierigkeitsgrad);
        schwierigkeitsgradAdapter = new ArrayAdapter<String>
                (this,
                        android.R.layout.simple_spinner_item,
                        new String[]{
                                "leicht", "mittel", "schwer"
                        });
        schwierigkeitsgradAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        schwierigkeitsgrad.setAdapter(schwierigkeitsgradAdapter);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            btnStart.setEnabled(false);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUESTCODE_PERMISSIONS);
        }
    }

    @Override
    public void onClick(View v) {
        int s = schwierigkeitsgrad.getSelectedItemPosition();
        if (v.getId() == R.id.btnStart) {
            Intent i = new Intent(this, GameActivity.class);
            i.putExtra("schwierigkeitsgrad", s);
            startActivityForResult(i, 1);
        } else if (v.getId() == R.id.btnSpeichern) {
            schreibeHighscoreName();
            highscoreAnzeigen();
            namenseingabe.setVisibility(View.INVISIBLE);
            internetHighscores(leseHighscoreName(), leseHighscore());
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        tv = findViewById(R.id.highscore);
        //tv.setText(Integer.toString(leseHighscore()));
        highscoreAnzeigen();
        internetHighscores("", 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode > leseHighscore()) {
                schreibeHighscore(resultCode);
                namenseingabe.setVisibility(View.VISIBLE);
            }
        }
    }

    private int leseHighscore() {
        SharedPreferences pref = getSharedPreferences("GAME", 0);
        return pref.getInt("HIGHSCORE", 0);
    }

    private void schreibeHighscore(int highscore) {
        SharedPreferences pref = getSharedPreferences("GAME", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("HIGHSCORE", highscore);

        editor.commit();
    }

    private void schreibeHighscoreName() {
        tv = (TextView) findViewById(R.id.spielerName);
        String name = tv.getText().toString().trim();
        SharedPreferences pref = getSharedPreferences("GAME", 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("HIGHSCORE_NAME", name);
        editor.commit();
    }

    private String leseHighscoreName() {
        SharedPreferences pref = getSharedPreferences("GAME", 0);
        return pref.getString("HIGHSCORE_NAME", "");
    }

    private void highscoreAnzeigen() {
        int highscore = leseHighscore();
        if (highscore > 0) {
            tv.setText(Integer.toString(highscore) + " von " + leseHighscoreName());
        } else {
            tv.setText("-");
        }
    }

    @Override
    public Drawable getDrawable(String name) {
        int id = getResources().getIdentifier(name, "drawable", this.getPackageName());
        Drawable d = getResources().getDrawable(id);
        d.setBounds(0, 0, 30, 30);
        return d;
    }

    private void internetHighscores(final String name, final int points) {
        List<String> highscoreList = new ArrayList<String>();

        (new Thread(() -> {
            try {
                url = new URL(HIGHSCORE_SERVER_BASE_URL
                        + "?game" + HIGHSCORESERVER_GAME_ID
                        + "&name" + URLEncoder.encode(name, "utf-8")
                        + "&points" + Integer.toString(points)
                        + "&max=100");

                HttpURLConnection conn = (HttpURLConnection) url.openConnection();

                InputStreamReader input = new InputStreamReader(conn.getInputStream(), "UTF8");

                BufferedReader reader = new BufferedReader(input, 2000);
                String line = reader.readLine();

                while (line != null) {
                    highscoreList.add(line);
                    line = reader.readLine();
                }


//                highscoresHtml = "";
//                for (String s : highscoreList) {
//                    highscoresHtml += "<b>"
//                            + s.replace(",", "</b> <font color='red'>")
//                            + "</font><img src='muecke'><br>";
//                }
//                Log.d("highscores", highscoresHtml);

            } catch (IOException e) {
                highscoresHtml = "Fehler: " + e.getMessage();
            }
            runOnUiThread(() -> {
                adapter = new TopListAdapter(this, 0, highscoreList);
                listView.setAdapter(adapter);
                adapter.notifyDataSetInvalidated();
            });
        })).start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUESTCODE_PERMISSIONS && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            btnStart.setEnabled(true);
        }
    }
}